# README #

Simple page to post lost, in adoption and found animals.

# Features #

* MEAN Stack

* HTML5

* Materialize

# Captures #

Main

![main.PNG](https://bitbucket.org/repo/xKrabx/images/3427210445-main.PNG)

Add / Edit

![add.PNG](https://bitbucket.org/repo/xKrabx/images/98482915-add.PNG)

Search

![search.PNG](https://bitbucket.org/repo/xKrabx/images/3592741802-search.PNG)

Moderation

![mod1.PNG](https://bitbucket.org/repo/xKrabx/images/3102272764-mod1.PNG)

![mod2.PNG](https://bitbucket.org/repo/xKrabx/images/22878665-mod2.PNG)

# Run #

Start the server with


```
#!javascript

node server.js > log.txt
```


The console.log instructions are written in the log.txt file