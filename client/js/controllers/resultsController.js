myApp.controller('resultsController', 
	function($scope, $stateParams, userFactory, postFactory){
		
		$scope.posts = $stateParams.posts;
		var query = $stateParams.query;
		var geolocation = $stateParams.geolocation;
		console.log(query);
		$scope.logged = false;
		$scope.user = null;
		
		userFactory.checkLogin(function(response){
			console.log("respuesta login: ", response);	
			if(response.data){
				$scope.logged = true;
				$scope.user = response.data;
			}
		});
		
		$scope.delete = function(post){
			var postID = post._id;
			postFactory.deletePost(postID, function(response){
				if (response.status == 200){
					removeElementFromPosts(postID);
				}
				else{
					Materialize.toast('Se produjo un error', 4000);
				}
			});
		};
		
		$scope.goEdition = function(){			
			$state.go('add', {});
		};
		
		$scope.report = function(post){
			if (post.reported){
				Materialize.toast('El post ya fue marcado como reportado', 4000);
			}
			else{
				postFactory.reportPost(post._id, 
					function(response){
						if (response.status == 200){
							Materialize.toast('El reporte fue enviado', 4000);
							post.reported = true;
						}
						else{
							Materialize.toast('Se produjo un error', 4000);
						}
					}
				);
			}
		};
		
		function removeElementFromPosts(postID){
			$scope.posts = $scope.posts.filter(
				function(returnableObjects){
					return returnableObjects._id !== postID;
				}
			);
		}
		
		/** SORT OF POSTS **/
		$scope.propertyName = 'creationDate';
		$scope.reverse = true;

		$scope.sortBy = function(propertyName) {
			$scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
			$scope.propertyName = propertyName;
		};
		
		/** Scrollfire load **/
	
		$scope.loadMorePosts = function(){
			var postsToSkip = $scope.posts.length;
			postFactory.searchPosts(query, postsToSkip, 
				function(response){					
					if (response.status == 200){
						var newPosts = response.data;					
						if (undefined !== newPosts && newPosts.length){
							newPosts.forEach(function(entry) {
								$scope.posts.push(entry);
							});
						}
						else {
							Materialize.toast('No hay mas resultados', 4000);
						}
					}
					else {
						Materialize.toast('Se produjo un error al cargar mas posts', 4000);
					}
				}
			)	
		}
				
	}
);