myApp.controller('readPostController',
	function($scope, $state, post, postFactory, userFactory){
				
		$scope.post = post.data.data;
		
		$scope.logged = false;
		$scope.user = null;
	  
		userFactory.checkLogin(function(response){
			console.log("respuesta login: ", response);	
			if(response.data){
				$scope.logged = true;
				$scope.user = response.data;
			}
		});
		
		$scope.delete = function(post){
			postFactory.deletePost(post._id, function(response){
				if (response.status == 200){
					$state.go('home', {});
				}
				else{
					Materialize.toast('Se produjo un error', 4000)
				}
			});
		};
		
	}
);