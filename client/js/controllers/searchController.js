myApp.controller('searchController',
	function($scope, $state, geolocationObject, userFactory, postFactory){
		
		$scope.user;
	
		userFactory.checkLogin(function(response){
			console.log("respuesta login: ", response);	
			if(response.data){
				$scope.logged = true;
				$scope.user = response.data;
			}
		});
		
		$scope.geolocationObject = geolocationObject;
		$scope.geolocationString = geolocationObject.city + ", "
									+ geolocationObject.countie + ", "
									+ geolocationObject.state + ", " 
									+ geolocationObject.country;
									
		$scope.publicationType = {
			value: "Seleccionar",
			choices: ["Seleccionar" ,"Perdido", "Encontrado", "En adopción"]
		};
		
		$scope.animalType = {
			value: "Seleccionar",
			choices: ["Seleccionar", "Perro", "Gato"]
		};
		
		$scope.sex = {
			name: 'male'		
		};
			
		$scope.animalSize = {
			value: "Seleccionar",
			choices: ["Seleccionar", "Pequeño", "Mediano", "Grande"]
		};
		
		$scope.animalFur = {
			value: "Seleccionar",
			choices: ["Seleccionar", "Pelo corto", "Pelo largo"]
		};
		
		$scope.animalNumberColors = {
			value: "Seleccionar",
			choices: ["Seleccionar", 1, 2, 3]
		};		
		
		var completeChoices = ["Color", "Blanco", "Negro", "Gris", "Marron oscuro", "Marron claro / Naranja", "Otro"];
		
		var choices1 = completeChoices.slice(0);
		$scope.animalColors1 = {
			value: "Seleccionar",
			choices: choices1
		};
		
		var choices2 = completeChoices.slice(0);
		$scope.animalColors2 = {
			value: "Seleccionar",
			choices: choices2
		};
		
		var choices3 = completeChoices.slice(0);
		$scope.animalColors3 = {
			value: "Seleccionar",
			choices: choices3
		};
		
		/*function checkAndSetVariable(value,query, variable){
			if (value == "Seleccionar"){
				console.log("asigno null");
				query.variable = null;
			}
			else{
				console.log("asigno valor");
				query.variable = value;
			}
		}*/
		
		$scope.searchPosts = function(){
			
			var query = {};
			query.publicationType = null;
			query.animalType = null;
			query.sex = null;
			query.animalSize = null;
			query.animalFur = null;
			query.animalNumberColors = null;
			query.color1 = null;
			query.color2 = null;
			query.color3 = null;
			
			query.geolocationObject = $scope.geolocationObject;			

			if ($scope.publicationType.value != "Seleccionar") query.publicationType = $scope.publicationType.value;
			if ($scope.animalType.value != "Seleccionar") query.animalType = $scope.animalType.value;
			if ($scope.sex.name != "Seleccionar") query.sex = $scope.sex.name;
			if ($scope.animalSize.value != "Seleccionar") query.animalSize = $scope.animalSize.value;
			if ($scope.animalFur.value != "Seleccionar") query.animalFur = $scope.animalFur.value;
			if ($scope.animalNumberColors.value != "Seleccionar") query.animalNumberColors = $scope.animalNumberColors.value;
			
			//Add colors
			switch ($scope.animalNumberColors.value){
				case '1':
						if ($scope.animalColors1.value != "Color"){
							query.color1 = $scope.animalColors1.value;
						}
						break;
				case '2':
						if ($scope.animalColors1.value != "Color"){
							query.color1 = $scope.animalColors1.value;
						}
						if ($scope.animalColors2.value != "Color"){
							query.color2 = $scope.animalColors2.value;
						}
						break;
				case '3':
						if ($scope.animalColors1.value != "Color"){
							query.color1 = $scope.animalColors1.value;
						}
						if ($scope.animalColors2.value != "Color"){
							query.color2 = $scope.animalColors2.value;
						}
						if ($scope.animalColors1.value != "Color"){
							query.color3 = $scope.animalColors3.value;
						}
						break;
			}
			
			console.log("search query");
			console.log(query);
			
			var postsToSkip = 0;
			
			postFactory.searchPosts(query, postsToSkip, function(response){
				
				if (response.status == 200){
					var results = response.data;
					if (undefined !== results && results.length){
						$state.go('results', { posts: results , query: query, geolocation: geolocationObject});
					}
					else {
						Materialize.toast('No se encontraron resultados para la busqueda', 4000);
					}
				}
				else {
					Materialize.toast('Se produjo un error en la busqueda', 4000);
				}
								
				
			})
		}
	}
)