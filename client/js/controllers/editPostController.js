myApp.controller('editPostController',
	function($scope, $state, userFactory, postFactory, post, Upload, resizeService, $log){
		
		var originalPost = post.data.data;		
		var geolocationObject = originalPost.geolocation;
		var resultImageURL, highResURL, thumbnailURL;
		
		$scope.post = post.data.data;
		highResURL = $scope.post.highResURL;
		thumbnailURL = $scope.post.thumbnailURL;
			
		$scope.geolocationString = geolocationObject.city + ", "
								+ geolocationObject.countie + ", "
								+ geolocationObject.state + ", " 
								+ geolocationObject.country;
								
		$scope.aditionalDescription = $scope.post.aditionalDescription;
		$scope.file = $scope.post.imageURL;		
								
		$scope.user;		
	
		userFactory.checkLogin(function(response){
			console.log("respuesta login: ", response);	
			if(response.data){
				$scope.logged = true;
				$scope.user = response.data;
			}
		});
		
		$scope.description = {};
		$scope.description.aditionalDescription = $scope.post.aditionalDescription;
		
		$scope.publicationType = {
			value: $scope.post.publicationType,
			choices: ["Perdido", "Encontrado", "En adopción"]
		};
		
		$scope.animalType = {
			value: $scope.post.animalType,
			choices: ["Perro", "Gato"]
		};
		
		$scope.sex = {
			name: $scope.post.sex		
		};
			
		$scope.animalSize = {
			value: $scope.post.animalSize,
			choices: ["Pequeño", "Mediano", "Grande"]
		};
		
		$scope.animalFur = {
			value: $scope.post.animalFur,
			choices: ["Pelo corto", "Pelo largo"]
		};
				
		$scope.animalNumberColors = {
			value: String($scope.post.animalNumberColors),
			choices: [1, 2, 3]
		};
		
		var completeChoices = ["Seleccionar", "Blanco", "Negro", "Gris", "Marron oscuro", "Marron claro / Naranja", "Otro"];
		
		var choices1 = ["Seleccionar", "Blanco", "Negro", "Gris", "Marron oscuro", "Marron claro / Naranja", "Otro"];
		$scope.animalColors1 = {
			value: $scope.post.color1,
			choices: choices1
		};
		
		var choices2 = ["Seleccionar", "Blanco", "Negro", "Gris", "Marron oscuro", "Marron claro / Naranja", "Otro"];
		$scope.animalColors2 = {
			value: $scope.post.color2,
			choices: choices2
		};
		
		var choices3 = ["Seleccionar", "Blanco", "Negro", "Gris", "Marron oscuro", "Marron claro / Naranja", "Otro"];
		$scope.animalColors3 = {
			value: $scope.post.color3,
			choices: choices3
		};
		
		$scope.editPost = function(){						
			if ($scope.user != null || $scope.user.access_level < 1){
				if ($scope.file != originalPost.imageURL){
					//the image has changed
					if ($scope.formEdit.file.$valid && $scope.file){
						if (validFields()){
							uploadHighResImage(resultImageURL);
						}
						else{
							Materialize.toast('Hay campos invalidos y/o sin seleccionar', 4000);
						}
					}
					else{
						Materialize.toast('La imagen no es valida', 4000);
					}				
				}
				else{					
					//the image is the same. dont upload it
					if (validFields()){
						editPost();
					}
					else{
						Materialize.toast('Hay campos invalidos y/o sin seleccionar', 4000);
					}
				}
			}
			else{
				Materialize.toast('Debe estar logueado', 4000);
			}
		}
		
		// str byteToHex(uint8 byte)
		//   converts a single byte to a hex string 
		function byteToHex(byte) {
			return ('0' + byte.toString(16)).slice(-2);
		}

		// str generateId(int len);
		//   len - must be an even number (default: 40)
		function generateId(len) {
			var arr = new Uint8Array((len || 40) / 2);
			window.crypto.getRandomValues(arr);
			return [].map.call(arr, byteToHex).join("");
		}
		
		function generateLocationPath(){
			return geolocationObject.country + "/"
					+ geolocationObject.state + "/"
					+ geolocationObject.countie + "/" 
					+ geolocationObject.city;
		}
		
		function generateThumbLocationPath(){
			return generateLocationPath() + "/thumbnails"
		}
		
		/**
			Upload a image to the server
		**/
		function uploadHighResImage (resultImageURL) {								
			var id = generateId();
			var locationFolder = generateLocationPath();
			var file = {};		
			file.id = id;
			file.locationFolder = locationFolder;
			Upload.upload({
				url: '/upload/',
				data: {'id': id, 'locationFolder': locationFolder, file: Upload.dataUrltoBlob(resultImageURL, "imageToUpload")}
				//data: {'id': id, 'locationFolder': locationFolder, file: file}
			}).then(function (resp) {
				//console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
				highResURL = resp.data;
				generateThumbnail(resultImageURL);		
			}, function (resp) {
				//console.log('Error status: ' + resp.status);
				Materialize.toast('Hubo un error al subir la imagen. Intente de nuevo', 4000);
			}, function (evt) {
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);				
			});
			
		};
		
		function generateThumbnail(resultImageURL){
			resizeService
				.resizeImage(resultImageURL, {
					height: 288,
					width: 288,
					size: 100, 
					sizeScale: 'ko'
					// Other options ... 288 288 de tamaños
				})
				.then(function(image){    
					var imageResized = new Image();
					imageResized.src = image;					
					uploadThumbnail(imageResized.src);

				})
				.catch($log.error); // Always catch a promise :)
		};
		
		function uploadThumbnail( imageURL ){
			var id = generateId();
			var locationFolder = generateThumbLocationPath();
			var file = {};		
			file.id = id;
			file.locationFolder = locationFolder;
			Upload.upload({
				url: '/upload/',
				data: {'id': id, 'locationFolder': locationFolder, file: Upload.dataUrltoBlob(imageURL, "thumbnailToUpload")}
				//data: {'id': id, 'locationFolder': locationFolder, file: file}
			}).then(function (resp) {
				//console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);			
				thumbnailURL = resp.data;
				editPost();
			}, function (resp) {
				//console.log('Error status: ' + resp.status);
				Materialize.toast('Hubo un error al subir la imagen. Intente de nuevo', 4000);
			}, function (evt) {
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);				
			});
		}
		
		function editPost(){
			var objectToSave = getObjectValues();
			
			postFactory.editPost(objectToSave, originalPost._id,
				function(response){
					if (response.status == 200){
						$state.go('home', {});
					}
					else{
						Materialize.toast('Se produjo un error al editar', 4000);
					}
				}
			);
			
		}
		
		/**
			Init the object that represents a post in the server
		**/
		function getObjectValues(){
			var post = {};
									
			post.publicationType = $scope.publicationType.value;		
			post.animalType = $scope.animalType.value;
			post.sex = $scope.sex.name;
			post.highResURL = highResURL;
			post.thumbnailURL = thumbnailURL;
			post.animalSize = $scope.animalSize.value;
			post.animalFur = $scope.animalFur.value;
			post.animalNumberColors = $scope.animalNumberColors.value;
			
			//Add colors
			switch ($scope.animalNumberColors.value){
				case '1':
						post.color1 = $scope.animalColors1.value;
						post.color2 = null;
						post.color3 = null;
						break;
				case '2':
						post.color1 = $scope.animalColors1.value;
						post.color2 = $scope.animalColors2.value;
						post.color3 = null;
						break;
				case '3':
						post.color1 = $scope.animalColors1.value;
						post.color2 = $scope.animalColors2.value;
						post.color3 = $scope.animalColors3.value;
						break;
			}
			
			post.aditionalDescription = $scope.description.aditionalDescription;
						
			console.log(post);
			
			return post;
		}
		
		/** checks if all values are selected **/	
		function validFields(){
			var response;
			response = $scope.publicationType.value != null && 						
						$scope.animalType.value != null &&						
						($scope.sex.name == 'male' || $scope.sex.name == 'female') &&				
						$scope.animalSize.value != null &&						
						$scope.animalFur.value != null &&						
						$scope.animalNumberColors.value != null;					
					
			console.log("response antes del switch "+response);
					
			if (response){
				switch ($scope.animalNumberColors.value){
					case '1':
							console.log("case 1");
							response = $scope.animalColors1.value != null &&
										$scope.animalColors1.value != "Seleccionar";
							break;
					case '2':
							console.log("case 2");
							response = $scope.animalColors1.value != null &&
										$scope.animalColors1.value != "Seleccionar" &&
										$scope.animalColors2.value != null &&
										$scope.animalColors2.value != "Seleccionar";
							break;
					case '3':
							console.log("case 3");
							response = $scope.animalColors1.value != null &&
										$scope.animalColors1.value != "Seleccionar" &&
										$scope.animalColors2.value != null &&
										$scope.animalColors2.value != "Seleccionar" &&
										$scope.animalColors3.value != null &&
										$scope.animalColors3.value != "Seleccionar";
							break;
				}
			}
			
			console.log("response dsp del switch "+response);
			
			return response;
			
		}
		
		$scope.updateResultImage = function(base64) {
			resultImage = base64;	  		
			$scope.$apply(); // Apply the changes.
			resultImageURL = resultImage;		
		};
		
		// Cropper API available when image is ready.
		$scope.cropperApi = function(api) {				
			resultImageURL = api.crop();		
			$scope.$apply(); // Apply the changes.
		};
		
	}
);