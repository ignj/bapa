myApp.controller('createPostController',
function ($scope, $http, $state, Upload, postFactory, geolocationSvc, geolocationObject, userFactory, resizeService, $log){	
	
	var resultImageURL, highResURL, thumbnailURL;
	
	$scope.geolocationObject = geolocationObject;
	$scope.geolocationString = geolocationObject.city + ", "
								+ geolocationObject.countie + ", "
								+ geolocationObject.state + ", " 
								+ geolocationObject.country;
								
	$scope.user;
	
	userFactory.checkLogin(function(response){
		console.log("respuesta login: ", response);	
		if(response.data){
			$scope.logged = true;
			$scope.user = response.data;
		}
	});	
	
	$scope.description = {};	

	$scope.publicationType = {
		value: "Seleccionar",
		choices: ["Seleccionar" ,"Perdido", "Encontrado", "En adopción"]
    };
	
	$scope.animalType = {
		value: "Seleccionar",
		choices: ["Seleccionar", "Perro", "Gato"]
	};
	
	$scope.sex = {
        name: 'male'		
    };
		
	$scope.animalSize = {
		value: "Seleccionar",
		choices: ["Seleccionar", "Pequeño", "Mediano", "Grande"]
	};
	
	$scope.animalFur = {
		value: "Seleccionar",
		choices: ["Seleccionar", "Pelo corto", "Pelo largo"]
	};
	
	$scope.animalNumberColors = {
		value: "Seleccionar",
		choices: ["Seleccionar", 1, 2, 3]
	};	
	
	var completeChoices = ["Seleccionar", "Blanco", "Negro", "Gris", "Marron oscuro", "Marron claro / Naranja", "Otro"];
	//var availableColors = completeChoices.slice(0);
	
	var choices1 = completeChoices.slice(0);
	$scope.animalColors1 = {
		value: "Seleccionar",
		choices: choices1
	};
	
	var choices2 = completeChoices.slice(0);
	$scope.animalColors2 = {
		value: "Seleccionar",
		choices: choices2
	};
	
	var choices3 = completeChoices.slice(0);
	$scope.animalColors3 = {
		value: "Seleccionar",
		choices: choices3
	};	
	
	/* usar ng-change en el tag select
	$scope.refreshAvailableColors = function(){
		var arrayChoices = completeChoices.slice(0);
		var color1, color2, color3;
		var index;
		
		if ($scope.animalColors1){
			color1 = $scope.animalColors1.value;
		}
		
		if ($scope.animalColors2){
			color2 = $scope.animalColors2.value;
		}
		
		if ($scope.animalColors3){
			color3 = $scope.animalColors3.value;
		}
		
		if (!(typeof color1 === 'undefined' || !color1) && color1 != "Seleccionar"){
			index = arrayChoices.indexOf(color1);
			if (index > -1) {
				arrayChoices.splice(index, 1);
			}
		}
		
		if (!(typeof color2 === 'undefined' || !color2) && color2 != "Seleccionar"){
			index = arrayChoices.indexOf(color2);
			if (index > -1) {
				arrayChoices.splice(index, 1);
			}
		}
		
		if (!(typeof color3 === 'undefined' || !color3) && color3 != "Seleccionar"){
			index = arrayChoices.indexOf(color3);
			if (index > -1) {
				arrayChoices.splice(index, 1);
			}
		}
		
		$scope.animalColors1.choices = arrayChoices;
		$scope.animalColors2.choices = arrayChoices;
		$scope.animalColors3.choices = arrayChoices;
	}
	*/
		
	
	//Add a new post
	function addPost (){		
	
		var objectToSave = getObjectValues();			
		
		postFactory.addPost(objectToSave, 
			function(response){
				if (response.status == 200){
					$state.go('home', {});
				}
				else{
					Materialize.toast('Se produjo un error al crear el post', 4000);
				}
			}
		);		
	}	
	
	/** checks if all values are selected **/	
	function validFields(){
		var response;
		response = $scope.geolocationObject != null &&				
				$scope.publicationType.value != null && 
				$scope.publicationType.value != "Seleccionar" && 
				$scope.animalType.value != null &&
				$scope.animalType.value != "Seleccionar" &&
				($scope.sex.name == 'male' || $scope.sex.name == 'female') &&				
				$scope.animalSize.value != null &&
				$scope.animalSize.value != "Seleccionar" &&
				$scope.animalFur.value != null &&
				$scope.animalFur.value != "Seleccionar" &&
				$scope.animalNumberColors.value != null &&
				$scope.animalNumberColors.value != "Seleccionar";
				
		console.log("response antes del switch "+response);
				
		if (response){
			switch ($scope.animalNumberColors.value){
				case '1':
						console.log("case 1");
						response = $scope.animalColors1.value != null &&
									$scope.animalColors1.value != "Seleccionar";
						break;
				case '2':
						console.log("case 2");
						response = $scope.animalColors1.value != null &&
									$scope.animalColors1.value != "Seleccionar" &&
									$scope.animalColors2.value != null &&
									$scope.animalColors2.value != "Seleccionar";
						break;
				case '3':
						console.log("case 3");
						response = $scope.animalColors1.value != null &&
									$scope.animalColors1.value != "Seleccionar" &&
									$scope.animalColors2.value != null &&
									$scope.animalColors2.value != "Seleccionar" &&
									$scope.animalColors3.value != null &&
									$scope.animalColors3.value != "Seleccionar";
						break;
			}
		}
		
		console.log("response dsp del switch "+response);
		
		return response;
		
	}
	
	
	/**
		Init the object that represents a post in the server
	**/
	function getObjectValues(){
		var post = {};
				
		post.date = new Date();
		post.geolocationObject = $scope.geolocationObject;
		post.user = $scope.user;		
		post.publicationType = $scope.publicationType.value;		
		post.animalType = $scope.animalType.value;
		post.sex = $scope.sex.name;
		post.highResURL = highResURL;
		post.thumbnailURL = thumbnailURL;
		post.animalSize = $scope.animalSize.value;
		post.animalFur = $scope.animalFur.value;
		post.animalNumberColors = $scope.animalNumberColors.value;
		
		//Add colors
		switch ($scope.animalNumberColors.value){
			case '1':
					post.color1 = $scope.animalColors1.value;
					post.color2 = null;
					post.color3 = null;
					break;
			case '2':
					post.color1 = $scope.animalColors1.value;
					post.color2 = $scope.animalColors2.value;
					post.color3 = null;
					break;
			case '3':
					post.color1 = $scope.animalColors1.value;
					post.color2 = $scope.animalColors2.value;
					post.color3 = $scope.animalColors3.value;
					break;
		}
		
		post.aditionalDescription = $scope.description.aditionalDescription;
		
		console.log("post a guardar");
		console.log(post);
		
		return post;
	}
	
	// str byteToHex(uint8 byte)
	//   converts a single byte to a hex string 
	function byteToHex(byte) {
		return ('0' + byte.toString(16)).slice(-2);
	}

	// str generateId(int len);
	//   len - must be an even number (default: 40)
	function generateId(len) {
		var arr = new Uint8Array((len || 40) / 2);
		window.crypto.getRandomValues(arr);
		return [].map.call(arr, byteToHex).join("");
	}
	
	function generateLocationPath(){
		return geolocationObject.country + "/"
				+ geolocationObject.state + "/"
				+ geolocationObject.countie + "/" 
				+ geolocationObject.city;
	}
	
	function generateThumbLocationPath(){
		return generateLocationPath() + "/thumbnails"
	}
	
	/**
		Upload a image to the server
	**/
    function uploadHighResImage (resultImageURL) {								
		var id = generateId();
		var locationFolder = generateLocationPath();
		var file = {};		
		file.id = id;
		file.locationFolder = locationFolder;
		Upload.upload({
			url: '/upload/',
			data: {'id': id, 'locationFolder': locationFolder, file: Upload.dataUrltoBlob(resultImageURL, "imageToUpload")}
			//data: {'id': id, 'locationFolder': locationFolder, file: file}
		}).then(function (resp) {
			//console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
			highResURL = resp.data;
			generateThumbnail(resultImageURL);		
		}, function (resp) {
			//console.log('Error status: ' + resp.status);
			Materialize.toast('Hubo un error al subir la imagen. Intente de nuevo', 4000);
		}, function (evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);				
		});
		
    };
	
	function uploadThumbnail( imageURL ){
		var id = generateId();
		var locationFolder = generateThumbLocationPath();
		var file = {};		
		file.id = id;
		file.locationFolder = locationFolder;
		Upload.upload({
			url: '/upload/',
			data: {'id': id, 'locationFolder': locationFolder, file: Upload.dataUrltoBlob(imageURL, "thumbnailToUpload")}
			//data: {'id': id, 'locationFolder': locationFolder, file: file}
		}).then(function (resp) {
			//console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);			
			thumbnailURL = resp.data;
			addPost();
		}, function (resp) {
			//console.log('Error status: ' + resp.status);
			Materialize.toast('Hubo un error al subir la imagen. Intente de nuevo', 4000);
		}, function (evt) {
			var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
			console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);				
		});
	}
	
	function generateThumbnail(resultImageURL){
			resizeService
				.resizeImage(resultImageURL, {
					height: 288,
					width: 288,
					size: 100, 
					sizeScale: 'ko'
					// Other options ... 288 288 de tamaños
				})
				.then(function(image){    
					var imageResized = new Image();
					imageResized.src = image;					
					uploadThumbnail(imageResized.src);

				})
				.catch($log.error); // Always catch a promise :)
	};
	
	$scope.addPost = function() {
		console.log($scope);
		if ($scope.user != null || $scope.user.access_level < 1){
			if ($scope.formAdd.file.$valid && $scope.file) {
				if (validFields()){					
					uploadHighResImage(resultImageURL);
				}
				else{
					Materialize.toast('Hay campos invalidos y/o sin seleccionar', 4000);
				}
			}
			else{
				Materialize.toast('Debe seleccionar una imagen', 4000);
			}
		}
		else{
			Materialize.toast('Debe estar logueado', 4000);
		}
    };
		
	
	
	/*
		
	$scope.myButtonLabels = {	  
		rotateLeft: ' (rotate left) ',
		rotateRight: ' (rotate right) ',
		zoomIn: ' (zoomIn) ',
		zoomOut: ' (zoomOut) ',
		fit: ' (fit) ',
		crop: ' <span class="btn btn-warning blue">[crop]</span> ' // You can pass html too.
	};*/
	
	$scope.updateResultImage = function(base64) {
		resultImage = base64;	  		
		$scope.$apply(); // Apply the changes.
		resultImageURL = resultImage;		
	};
	
	// Cropper API available when image is ready.
	$scope.cropperApi = function(api) {				
		resultImageURL = api.crop();		
		$scope.$apply(); // Apply the changes.
	};
	
});