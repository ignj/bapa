myApp.controller('mainController', 
function($scope, $location, $http, $window, userFactory, postFactory, $state, geolocationObject, posts){
	
	$scope.posts = posts.data.data;

	$scope.geolocationObject = geolocationObject;
	$scope.geolocationString = geolocationObject.city + ", "
								+ geolocationObject.countie + ", "
								+ geolocationObject.state + ", " 
								+ geolocationObject.country;
								
	$scope.registeredUser = {};
	$scope.error = {};
	$scope.logged = false;
	$scope.user = null;
  
	userFactory.checkLogin(function(response){
		console.log("respuesta login: ", response);	
		if(response.data){
			$scope.logged = true;
			$scope.user = response.data;			
		}
	});
  
	$scope.logoutUser = function(){
		userFactory.logoutUser(function(response){
			if (response.status = 200){
				//se deslogueo correctamente
				$scope.user = null;
				$scope.logged = false;
				$window.location.reload();
			}	  
		});
	};
  
	$scope.goEdition = function(){
		$state.go('add', {});
	};
	
	$scope.goSearch = function(){
		$state.go('search', {});
	}
	
	$scope.goHelp = function(){
		$state.go('help', {});
	}
	
	$scope.delete = function(post){
		var postID = post._id;
		postFactory.deletePost(postID, function(response){
			if (response.status == 200){
					removeElementFromPosts(postID);
			}
			else{
				Materialize.toast('Se produjo un error', 4000);
			}
		});
	};
	
	$scope.report = function(post){
		if (post.reported){
			Materialize.toast('El post ya fue marcado como reportado', 4000);
		}
		else{
			postFactory.reportPost(post._id, 
				function(response){
					Materialize.toast('El reporte fue enviado', 4000);
					post.reported = true;
				}
			);
		}
	};
	
	function removeElementFromPosts(postID){
		$scope.posts = $scope.posts.filter(
			function(returnableObjects){
				return returnableObjects._id !== postID;
			}
		);
	}
	
	/** SORT OF POSTS **/
	$scope.propertyName = 'creationDate';
	$scope.reverse = true;

	$scope.sortBy = function(propertyName) {
		$scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
		$scope.propertyName = propertyName;
	};
	
	/** Scrollfire load **/
	
	$scope.loadMorePosts = function(){				
		postFactory.getPosts(geolocationObject._id, $scope.posts.length)
			.then(function(response){				
				if (response.data.status == 200){
					var newPosts = response.data.data;
					//console.log(newPosts);
					if (undefined !== newPosts && newPosts.length){
						newPosts.forEach(function(entry) {
							$scope.posts.push(entry);
						});
					}
					else{
						Materialize.toast('No hay mas resultados', 4000);
					}
				}
				else {
					Materialize.toast('Se produjo un error al cargar mas posts', 4000);
				}
			}
		);		
	}
 
});
