myApp.controller('indexController', 
function($scope, $location, $http, $window, userFactory, $state){
	
	$scope.registeredUser = {};
	$scope.error = {};
	$scope.logged = false;
	$scope.user = null;
  
	userFactory.checkLogin(function(response){
		console.log("respuesta login: ", response);	
		if(response.data){
			$scope.logged = true;
			$scope.user = response.data;
		}
	});
  
	$scope.logoutUser = function(){
		userFactory.logoutUser(function(response){
			if (response.status = 200){
				//se deslogueo correctamente
				$scope.user = null;
				$scope.logged = false;
				$window.location.reload();      
			}	  
		});
	};
	
	$scope.goModeration = function(){
		$state.go('moderation', {});
	}
	
	$scope.goHelp = function(){
		$state.go('help', {});
	}
	
})