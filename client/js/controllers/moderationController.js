myApp.controller('moderationController',
	function($scope, $window, posts, postFactory, userFactory){
				
		$scope.posts = posts.data.data;
		
		$scope.logged = false;
		$scope.user = null;
		
		$scope.userInfo = {};
		$scope.target = {};
	  
		userFactory.checkLogin(function(response){
			console.log("respuesta login: ", response);	
			if(response.data){
				$scope.logged = true;
				$scope.user = response.data;
			}
		});
		
		$scope.delete = function(post){
			var postID = post._id;
			postFactory.deletePost(postID, function(response){
				if (response.status == 200){
					removeElementFromPosts(postID);
					Materialize.toast('Eliminado', 4000);
				}
				else{
					Materialize.toast('Se produjo un error en la operacion', 4000);
				}
			});
		};
		
		$scope.unreport = function(post){
			var postID = post._id;			
			postFactory.unreport(postID, function(response){
				console.log(response);
				if (response.status == 200){
					removeElementFromPosts(postID);
					Materialize.toast('Status modificado', 4000);
				}
				else{
					Materialize.toast('Se produjo un error en la operacion', 4000);
				}
			});
		}
		
		$scope.getUserData = function(userInfo){
			console.log(userInfo);
			userFactory.getUserData(userInfo.username, 
				function(response){
					if (response.status == 200){
						$scope.userObserved = response.data[0];
					}
					else{
						Materialize.toast('Se produjo un error en la operacion', 4000);
					}
				}
			);
		}
		
		$scope.setAccessLevel = function(userObserved, target){
			var targetAccess = target.access_level;
			var userID = userObserved._id;
			console.log(targetAccess);
			console.log(userID);
			userFactory.setUserAccess(userID, targetAccess, 
				function(response){
					console.log(response);
					if (response.status == 200)
						Materialize.toast('Se realizaron las modificaciones correctamente', 4000);
					else
						Materialize.toast('Se produjo un error en la operacion', 4000);
				}
			)
		}
		
		function removeElementFromPosts(postID){
			$scope.posts = $scope.posts.filter(
				function(returnableObjects){
					return returnableObjects._id !== postID;
				}
			);
		}
		
		/** Scrollfire load **/
	
		$scope.loadMorePosts = function(){
			var postsToSkip = $scope.posts.length;
			console.log(postsToSkip);
			postFactory.getReported(postsToSkip)
				.then(function(response){
					console.log(response);
					if (response.data.status == 200){
						var newPosts = response.data.data;
						if (undefined !== newPosts && newPosts.length){
							newPosts.forEach(function(entry) {
								$scope.posts.push(entry);
							});
						}
						else{
							Materialize.toast('No hay mas resultados', 4000);
						}
					}
					else {
						Materialize.toast('Se produjo un error al cargar mas posts', 4000);
					}
				}
			);		
		}
		
	}
)