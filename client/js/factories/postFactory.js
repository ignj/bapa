myApp.factory('postFactory', function($http, $window, $location, $stateParams){
	
	var o = {
		posts: []
	};
		
	o.addPost = function (input, callback){		
		return $http.post('/posts', input)
			.success(function(data){				
				o.posts.push(data);
				callback(data);
			});
	};	
	
	o.getPosts = function(geolocationId, postsToSkip){
		
		//var geoId = geolocationId._id;
		var config = {
			params: {
				geolocationId: geolocationId,
				postsToSkip: postsToSkip
			}
		};		
		//console.log(config);
		return $http.get('/posts', config)
			.success(function (data){
				angular.copy(data, o.posts);
			});
	};
	
	o.getPost = function(id){
		console.log("postfactory "+id);
		return $http.get('/posts/'+id)
			.success(function (res){
				console.log("GETPOST");
				console.log(res);
				return res;
			});
	};
	
	o.editPost = function(input, id, callback){
		return $http.patch('/posts/' + id, input)
			.success(function (data){
				callback(data);
			})
	}
	
	o.deletePost = function(id, callback){
		return $http.delete('/posts/' + id)
			.success(function (data){
				callback(data);
			})
	}
	
	o.searchPosts = function(input, postsToSkip , callback){
		return $http.get('/search', {params: { query: input , postsToSkip: postsToSkip}})
			.success(function (data){
				console.log("vuelvo del server");
				console.log(data);
				callback(data);
			});
	}
	
	o.reportPost = function(postID, callback){
		console.log("REPORTO "+postID);
		var config = { postID: postID };
		return $http.post('/reported', config)
			.success(
				function(response){
					callback(response);
				}
			);
	};
	
	o.getReported = function(postsToSkip){
		var config = {
			params: {
				postsToSkip: postsToSkip 
			}
		};
		return $http.get('/reported', config)
			.success(
				function(data){
					console.log(data);
					angular.copy(data, o.posts);
				}
			)		
	};
	
	o.unreport = function(postID, callback){
		var config = { postID: postID };
		return $http.patch('/reported', config)
			.success(
				function(data){
					callback(data);
				}
			)
	}
		
	return o;
	
});