myApp.factory('geolocationSvc', ['$q', '$window', '$http', function ($q, $window, $http) {

    'use strict';
	
	return{
		
		getCurrentPosition: function() {
			var deferred = $q.defer();

			if (!$window.navigator.geolocation) {
				deferred.reject('Geolocation not supported.');
			} else {
				$window.navigator.geolocation.getCurrentPosition(
					function (position) {
						deferred.resolve(position);						
					},
					function (err) {
						deferred.reject(err);
					});
			}
		
			return deferred.promise;
		},
		
		codeLatLng: function(lat, lng) {
			var deferred = $q.defer();
			var geocoder = new google.maps.Geocoder();
			var latlng = new google.maps.LatLng(lat, lng);
			geocoder.geocode({'latLng': latlng}, function(results, status) {
				if (status == google.maps.GeocoderStatus.OK) {					
					if (results[1]) {
						
						//console.log(results);
						var addressComponents = results[0].address_components;
						var city, countie, state, country;
												
						
						addressComponents.forEach(
							function(entry){
								if (entry.hasOwnProperty('types') && entry.types[0]){
									switch (entry.types[0]){
										case "locality":
											city = entry.long_name;
											break;
										case "administrative_area_level_2":
											countie = entry.long_name;
											break;
										case "administrative_area_level_1":
											state = entry.long_name;
											break;
										case "country":
											country = entry.long_name;
											break;
									}
								}
							}
						);
						
						/*var locationObject = '{ "city":\"'+results[0].address_components[3].long_name+'\" , '
											+' "countie":\"'+results[0].address_components[4].long_name+'\" , '
											+' "state":\"'+results[0].address_components[5].long_name+'\" , '
											+' "country":\"'+results[0].address_components[6].long_name+'\" } ';*/
											
						var locationObject = '{ "city":\"'+city+'\" , '
											+' "countie":\"'+countie+'\" , '
											+' "state":\"'+state+'\" , '
											+' "country":\"'+country+'\" } ';
											
						deferred.resolve(JSON.parse(locationObject));
				
					} 
					else {
						deferred.reject("No results found");
					}
				}
				else {
					deferred.reject("Geocoder failed due to: " + status);
				}
			});
			return deferred.promise;
		},
		
		getGeolocation: function(geolocationObject){
			return $http.get('/geolocation', {params: { geolocationObject: geolocationObject}})
				.success(function (data){
					console.log(data);
					return data;
				});			
		}
	}
}]);