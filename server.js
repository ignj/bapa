// Require the Express Module
var express = require("express"),

//middleware
    path = require("path"),
    mongoose = require('mongoose'),
    app = express();
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');

//passport and strategies
var passport = require('passport');
var passportLocal = require('passport-local');

//certificates
var fs = require('fs');
var http = require('http');
var https = require('https');
var privateKey  = fs.readFileSync('sslcert/localhost.key', 'utf8');
var certificate = fs.readFileSync('sslcert/localhost.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate};

// middleware configuration
app.use(bodyParser.json());
app.use(cookieParser());
app.use(expressSession({ secret: process.env.SESSION_SECRET || 'solegood',
                          resave: false,
                          saveUninitialized: false}));
						  
//passport configuration
app.use(passport.initialize());
app.use(passport.session());
require('./server/config/mongoose.js');

require('./server/config/passport.js');

// authentication
require("./server/auth/fb_auth.js");

//routes
require('./server/config/routes.js')(app);

//image uploading
require('./server/config/imageUploader.js')(app);

// set up a static file server that points to the "client" directory
app.use(express.static(path.join(__dirname, './client')));
app.use(express.static(__dirname + '/public'));
//set up path for external libraries
app.use('/scripts', express.static(__dirname + '/node_modules/materialize-css/dist/'));
app.use('/scripts', express.static(__dirname + '/node_modules/angular-ui-router/release'));
app.use('/scripts', express.static(__dirname + '/node_modules/ng-file-upload/dist'));
app.use('/scripts', express.static(__dirname + '/node_modules/angular-image-cropper/dist'));
app.use('/scripts', express.static(__dirname + '/node_modules/angular-images-resizer'));

//start server
var httpServer = http.createServer(app);
//var httpsServer = https.createServer(credentials, app);

httpServer.listen(8000, function () { console.log('HTTP BAPA on: 8000') } );
//httpsServer.listen(8000, function () { console.log('HTTPS BAPA on: 8000') });