var mongoose = require('mongoose');
var Geolocation = mongoose.model('Geolocation');

module.exports = (function(){
	return{
		
		getGeolocation: function(req,res){
			//console.log("GET GEOLOCATION");
			var geolocationParam = JSON.parse(req.query.geolocationObject);
			
			//GETS GEOLOCATION ID IF EXISTS
			var attrCity = geolocationParam.city;
			var attrCountie = geolocationParam.countie;
			var attrState = geolocationParam.state;
			var attrCountry = geolocationParam.country;				
			var geolocationId;
			
			Geolocation.findOne({city: attrCity,
								countie: attrCountie,
								state: attrState,
								country: attrCountry},
								"_id city countie state country",
								function(err, geolocation){
										if (err) res.send(500);
																				
										if (geolocation == null) { //not found
												//console.log("LOCALIZACION NO ENCONTRADA");
												res.json(geolocationParam);
										}
										else { //found
											res.json(geolocation)
										}
								}
							);
		}
		
	}
})();