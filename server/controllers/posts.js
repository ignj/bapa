var mongoose = require('mongoose');
var Post = mongoose.model('Post');
var Geolocation = mongoose.model('Geolocation');

var User = mongoose.model('User');
var assert = require('assert');

var SecurityQueries = require ('../utils/securityQueries');
var LogQueries = require('../utils/logQueries');

module.exports = (function(){
	return{
		
		createPost: function(req, res){
			
			if (req.user == null){
				//Error if user is not logged in
				console.log("createPost", "Intenta crear post sin estar logueado", "IP: " + LogQueries.getIP(req));
				res.status(400).json({status: 400, data: null});				
				return;
			}
			
			if (req.user.access_level < 1){
				console.log("createPost", "Intenta crear post sin tener privilegios", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username);
				res.status(400).json({status: 400, data: null});				
				return;
			}
			
			var attrCreationDate = req.body.creationDate;			
			var attrPublicationType = req.body.publicationType;
			var attrAnimalType = req.body.animalType;
			var attrSex = req.body.sex;
			var attrHighResURL = req.body.highResURL;
			var attrThumbnailURL = req.body.thumbnailURL;
			var attrAnimalSize = req.body.animalSize;
			var attrAnimalFur = req.body.animalFur;
			var attrAnimalNumberColors = req.body.animalNumberColors;
			var attrColor1 = req.body.color1;
			var attrColor2 = req.body.color2;
			var attrColor3 = req.body.color3;
			var attrAditionalDescription = req.body.aditionalDescription;
			
			if (!SecurityQueries.isValidPublicationType(attrPublicationType) ||
				!SecurityQueries.isValidAnimalType(attrAnimalType) ||
				!SecurityQueries.isValidAnimalSex(attrSex) ||
				!SecurityQueries.isValidAnimalSize(attrAnimalSize) ||
				!SecurityQueries.isValidAnimalFur(attrAnimalFur) ||
				!SecurityQueries.isValidAnimalNumberColors(attrAnimalNumberColors) ||
				!SecurityQueries.isValidAnimalColor(attrColor1) ||
				!SecurityQueries.isValidAnimalColor(attrColor2) ||
				!SecurityQueries.isValidAnimalColor(attrColor3) ||
				!SecurityQueries.isValidAditionalDescription(attrAditionalDescription)
				)
				{
					console.log("createPost", "Intenta escribir post con campos invalidos", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username);
					res.status(400).json({status: 400, data: null});					
					return;
				}
						
						
			// GEOLOCATION
			var attrCity = req.body.geolocationObject.city;
			var attrCountie = req.body.geolocationObject.countie;
			var attrState = req.body.geolocationObject.state;
			var attrCountry = req.body.geolocationObject.country;			
			var geolocationId;
												
			var queryGeolocation = 
				Geolocation.findOne({city: attrCity,
									countie: attrCountie,
									state: attrState,
									country: attrCountry},
									"_id",
									function(err, geolocation){
										if (err) {											
											res.status(500).json({status: 500, data: null});											
											return;
										}
																				
										if (geolocation == null) { //not found
											//console.log("LOCALIZACION NO ENCONTRADA");
											var geolocationObject = new Geolocation({
												city: attrCity,
												countie: attrCountie,		
												state: attrState,
												country: attrCountry
											});
											geolocationId = geolocationObject._id;
											
											geolocationObject.save(								
												function (err, savedObject){
													if (err){ 
														res.status(500).json({status: 500, data: null});														
														return;
													}
												}
											);	
											
										}
										else { //found
											//console.log("FOUND");
											geolocationId = geolocation._id;
										}
									}
								);
								
			// Use native promises
			mongoose.Promise = global.Promise;
			assert.equal(queryGeolocation.exec().constructor, global.Promise);
			
			queryGeolocation.then(function(doc){					
					// User 
					var attrId = req.body.user._id;
					var attrUsername = req.body.user.username;					
					var userId;
					//console.log("antes de queryUser");
					var queryUser = User.findOne({
						_id: attrId
						},
						"username",
						function(err, userFromDB){
							if (err) {
								res.status(400).json({status: 400, data: null});								
								return;
							}
							
							if (userFromDB == null){
								// not found
								console.log("createPost", "Usuario inexistente quiere crear post [FAKE USER._ID]", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username);
								//LOG THIS
								res.status(400).json({status: 400, data: null});								
								return;
							}
							/*else { 
								//found
								console.log("USUARIO ENCONTRADO");
								if (!SecurityQueries.isOwner(userFromDB._id, attrId)){
									//LOG THIS
									LogQueries.writeLog("createPost", "El usuario que quiere crear el post no existe en la base de datos", req, req.user);
									res.status(400).json({status: 400, data: null});									
									return;
								}
							}	*/										
						}
					);
					
					mongoose.Promise = global.Promise;
					assert.equal(queryUser.exec().constructor, global.Promise);
					queryUser.then(function(doc){						
						
						//SAVE NEW POST
						var post = new Post({
							creationDate: attrCreationDate,
							geolocation: geolocationId,
							user: attrId,
							publicationType: attrPublicationType,
							animalType: attrAnimalType,
							sex: attrSex,
							highResURL: attrHighResURL,
							thumbnailURL: attrThumbnailURL,
							animalSize: attrAnimalSize,
							animalFur: attrAnimalFur,
							animalNumberColors: attrAnimalNumberColors,
							color1: attrColor1,
							color2: attrColor2,
							color3: attrColor3,
							aditionalDescription: attrAditionalDescription
						});
						
						post.save(function (err, post){
							if (err){
								res.status(500).json({status: 500, data: null});
								return;
							}
							else{								
								console.log("createPost", "Crea post exitosamente", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username);
								res.status(200).json({status: 200, data: post});
								return;
							}
						});
						
					});
					
				}
			)				
		},
		
		getPosts: function(req, res){			
			
			var geolocationId = req.query.geolocationId;
			var postsToSkip = req.query.postsToSkip;
			//console.log("Geolocation ID "+geolocationId);
			//console.log("POSTS TO SKIP "+postsToSkip);
			
			if (geolocationId == false){
				res.status(200).json({status: 200, data: "[]"});				
				return;
			}
			
			Post.find({geolocation: geolocationId})
				.select({ _id: 1,
						creationDate: 1,
						user: 1,
						publicationType: 1,
						animalType: 1,
						sex: 1,
						thumbnailURL: 1,
						animalSize: 1,
						animalFur: 1,
						animalNumberColors: 1,
						color1: 1,
						color2: 1,
						color3: 1,
						aditionalDescription: 1,
						reported: 1
						})
				.sort({creationDate: 'desc'})
				.skip((Number(postsToSkip)))
				.limit(12)
				.populate('user', 'username name pictureURL -_id')
				.exec(function(err, results){
					if (err){
						res.status(500).json({status: 500, data: null});						
						return;
					}
					else{								
						//console.log(results);
						res.status(200).json({status: 200, data: results});						
						return;
					}
				});
			
		},
		
		getPostById: function(req, res, next, id){
			//console.log("GETPOSTBYID "+id);
			Post.findById(id)
				.populate('user geolocation')
				.exec(function(err,post){					
					if (err) {	
						res.status(500).json({status: 500, data: null});						
						return;						
					}
					if (!post) {
						res.status(404).json({status: 404, data: null});
						return;
					}
										
					//res.json(post);
					req.originalPost = post;
					return next();
				});
		},
		
		removeUserInformation: function(req, res){			
			
			var originalPost = req.originalPost;
			var postToReturn = {};
			
			postToReturn._id = originalPost._id;
			postToReturn.geolocation = originalPost.geolocation;
			
			postToReturn.user = {};
			postToReturn.user.username = originalPost.user.username;
			postToReturn.user.name = originalPost.user.name;
			postToReturn.user.pictureURL = originalPost.user.pictureURL;
			
			postToReturn.publicationType = originalPost.publicationType;
			postToReturn.animalType = originalPost.animalType;
			postToReturn.animalFur = originalPost.animalFur;
			postToReturn.sex = originalPost.sex;
			postToReturn.highResURL = originalPost.highResURL;
			postToReturn.thumbnailURL = originalPost.thumbnailURL;
			postToReturn.animalSize = originalPost.animalSize;
			postToReturn.animalNumberColors = originalPost.animalNumberColors;
			postToReturn.color1 = originalPost.color1;
			postToReturn.color2 = originalPost.color2;
			postToReturn.color3 = originalPost.color3;
			postToReturn.creationDate = originalPost.creationDate;
			postToReturn.aditionalDescription = originalPost.aditionalDescription;
			postToReturn.reported = originalPost.reported;
						
			res.status(200).json({status: 200, data: postToReturn});
			//console.log(postToReturn);
			
			
		},
		
		patchPost: function(req, res, next){
			//console.log("PATCH BY ID");			
									
			var originalPost = req.originalPost;
			var newPost = req.body;
			var user = req.user;
			
			var attrPublicationType = newPost.publicationType;
			var attrAnimalType = newPost.animalType;
			var attrSex = newPost.sex;
			var attrHighResURL = newPost.highResURL;
			var attrThumbnailURL = newPost.thumbnailURL;
			var attrAnimalSize = newPost.animalSize;
			var attrAnimalFur = newPost.animalFur;
			var attrAnimalNumberColors = newPost.animalNumberColors;
			var attrColor1 = newPost.color1;
			var attrColor2 = newPost.color2;
			var attrColor3 = newPost.color3;
			var attrAditionalDescription = newPost.aditionalDescription;
			
			if (user == null){
				console.log("patchPost","Intenta operar con user null", "IP: " + LogQueries.getIP(req));
				res.status(400).json({status: 400, data: null});
				return;
			}
			
			if (user.access_level < 1){
				console.log("patchPost","Intenta operar sin privilegios", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username, "Post._id: " + originalPost._id);
				res.status(400).json({status: 400, data: null});				
				return;
			}
			
			if (!SecurityQueries.isValidPublicationType(attrPublicationType) ||
				!SecurityQueries.isValidAnimalType(attrAnimalType) ||
				!SecurityQueries.isValidAnimalSex(attrSex) ||
				!SecurityQueries.isValidAnimalSize(attrAnimalSize) ||
				!SecurityQueries.isValidAnimalFur(attrAnimalFur) ||
				!SecurityQueries.isValidAnimalNumberColors(attrAnimalNumberColors) ||
				!SecurityQueries.isValidAnimalColor(attrColor1) ||
				!SecurityQueries.isValidAnimalColor(attrColor2) ||
				!SecurityQueries.isValidAnimalColor(attrColor3) ||
				!SecurityQueries.isValidAditionalDescription(attrAditionalDescription)
				)
				{
					console.log("patchPost","Envia campos invalidos", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username, "Post._id:" + originalPost._id);
					res.status(400).json({status: 400, data: null});					
					return;
				}
						
			if (SecurityQueries.isOwner(originalPost.user._id, user._id)){
				Post.update({ _id: originalPost._id },
							{ $set: 
								{ 	publicationType: attrPublicationType,
									animalType: attrAnimalType,
									sex: attrSex,
									highResURL: attrHighResURL,
									thumbnailURL: attrThumbnailURL,
									animalSize: attrAnimalSize,
									animalFur: attrAnimalFur,
									animalNumberColors: attrAnimalNumberColors,
									color1: attrColor1,
									color2: attrColor2,
									color3: attrColor3,
									aditionalDescription: attrAditionalDescription
								}
							},
							function(err){
								if (err) {
									res.status(400).json({status: 400, data: null});									
									return;
								}
								console.log("patchPost","Modifica post exitosamente", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username, "Post._id:" + originalPost._id);
								res.status(200).json({status: 200, data: null});
								return;
							}
							)
			}			
			else {
				//TODO: LOG THIS ERROR
				console.log("patchPost","Usuario que no es dueño del post intenta modificarlo [FAKE USER._ID]", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username, "Post._id:" + originalPost._id);
				res.status(400).json({status: 400, data: null});				
				return;
			}
			
		},
		
		deletePost: function(req, res, next){
			//console.log("DELETE POST");
			
			var user = req.user;
			var originalPost = req.originalPost;
			
			if ( user == null ){
				console.log("deletePost", "Intenta operar con usuario null", "IP: " + LogQueries.getIP(req));
				res.status(400).json({status: 400, data: null});				
				return;
			}
			
			if (user.access_level < 1){
				console.log("deletePost", "Intenta operar sin privilegios", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username, "Post._id:" + originalPost._id);
				res.status(400).json({status: 400, data: null});				
				return;
			}
			
			if (SecurityQueries.isOwner(originalPost.user._id, user._id ) || (user.access_level >= 2) ){
				Post.remove({ _id: originalPost._id },
							function(err){
								if (err) {									
									res.status(500).json({status: 500, data: null});
									return;
								}
								
								console.log("deletePost", "Elimina post exitosamente", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username, "Post._id:" + originalPost._id);
								res.status(200).json({status: 200, data: null});
								return;
							}
							)
			}
			else {
				console.log("deletePost","Usuario que no es dueño ni mod intenta borrar post [FAKE USER]", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username, "Post._id:" + originalPost._id);
				res.status(400).json({status: 400, data: null});				
				return;
			}
			
		},
		
		searchPosts: function(req, res){
			//console.log("SEARCH");
			
			var queryParameters = JSON.parse(req.query.query);
			var postsToSkip = req.query.postsToSkip;			
			//console.log("query: ");
			//console.log(queryParameters);
			
			
			/* FIND GEOLOCATION */
			var attrCity = queryParameters.geolocationObject.city;
			var attrCountie = queryParameters.geolocationObject.countie;
			var attrState = queryParameters.geolocationObject.state;
			var attrCountry = queryParameters.geolocationObject.country;	
			//console.log(attrCountry);
			var geolocationId;
												
			var queryGeolocation = 
				Geolocation.findOne({city: attrCity,
									countie: attrCountie,
									state: attrState,
									country: attrCountry},
									"_id",
									function(err, geolocation){
										if (err) {
											res.status(500).json({status: 500, data: null});
											return;
										}
																				
										if (geolocation == null) { //not found
												//console.log("LOCALIZACION NO ENCONTRADA");
												res.status(200).json({status: 200, data: "[]"});
												return;
										}
										else { //found
											//console.log("FOUND");
											geolocationId = geolocation._id;
										}
									}
								);
								
			// Use native promises
			mongoose.Promise = global.Promise;
			assert.equal(queryGeolocation.exec().constructor, global.Promise);
			
			queryGeolocation.then(function(doc){				
				var queryDB = Post.find();
				//console.log(queryParameters);
				
				//select fields
				queryDB.select('_id user animalNumberColors publicationType sex animalType animalFur animalSize color1 color2 color3 creationDate thumbnailURL aditionalDescription reported');
				
				//Add geolocation parameter
				queryDB.where('geolocation').equals(geolocationId);
				
				//Add publicationType parameter
				if (queryParameters.publicationType != null){
					queryDB.where('publicationType').equals(queryParameters.publicationType);
				}
				
				//Add animalType parameter
				if (queryParameters.animalType != null){
					queryDB.where('animalType').equals(queryParameters.animalType);
				}

				//Add animalSize parameter
				if (queryParameters.animalSize != null){
					queryDB.where('animalSize').equals(queryParameters.animalSize);
				}
				
				//Add animalFur parameter
				if (queryParameters.animalFur != null){
					queryDB.where('animalFur').equals(queryParameters.animalFur);
				}
				
				//Add sex parameter
				if (queryParameters.sex != null){
					queryDB.where('sex').equals(queryParameters.sex);
				}					
				
				function addColor1(){					
					if (queryParameters.color1 != null){							
							queryDB.where('color1').equals(queryParameters.color1);
					}
				};
				
				function addColor2(){			
					if (queryParameters.color2 != null){
							queryDB.where('color2').equals(queryParameters.color2);
					}
				};
				
				function addColor3(){
					if (queryParameters.color3 != null){
							queryDB.where('color3').equals(queryParameters.color3);
					}
				};
				
				//Add animalNumberColors parameter
				switch (Number(queryParameters.animalNumberColors)){
					case 1:
						queryDB.where('animalNumberColors').equals(1);
						
						//Add color1 parameter
						addColor1();						
							
						break;
					case 2:
						queryDB.where('animalNumberColors').equals(2);
						
						//Add color1 parameter
						addColor1();
						
						//Add color2 parameter
						addColor2();
						
						break;
					case 3:
						queryDB.where('animalNumberColors').equals(3);
						
						//Add color1 parameter
						addColor1();
						
						//Add color2 parameter
						addColor2();
						
						//Add color3 parameter
						addColor3();
						
						break;
					default:						
						break;
				}
				
				queryDB.populate('user', 'username name pictureURL -_id');
				queryDB.sort({creationDate: 'desc'});
				queryDB.skip(Number(postsToSkip));
				queryDB.limit(12);
				
				queryDB.exec(function(err,posts){
					if (err) {
						res.status(500).json({status: 500, data: null});
						return;
					}	
					
					res.status(200).json({status: 200, data: posts});
					return;					
					
				});
			});
		},
		
		reportPost : function(req, res){
			//console.log("REPORT POST");
			
			var postID = req.body.postID;
			
			if (req.user == null){
				console.log("reportPost", "Intenta operar con usuario null", "IP: " + LogQueries.getIP(req));
				res.status(400).json({status: 400, data: null});
				return;
			}
			
			if (req.user.access_level < 1){
				console.log("reportPost", "Intenta operar sin suficientes privilegios", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username, "Post._id:" + postID);
				res.status(400).json({status: 400, data: null});
				return;
			}
			
			//LOG THE USER THAT REPORTS
			
			Post.update({ _id: postID },
						{ $set: { reported : true } },
						function (err){
							if (err) {
								res.status(500).json({status: 500, data: null});								
								return;
							}
			
							console.log("reportPost", "Reporta post", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username, "Post._id:" + postID);
							res.status(200).json({status: 200, data: null});							
							return;
						}
						)
		},
		
		getReportedPosts : function(req, res){			
			//console.log("GET REPORTED");
			var postsToSkip = req.query.postsToSkip;			
			
			Post.find({ reported: true })
				.select({ _id: 1,
						creationDate: 1,
						user: 1,
						publicationType: 1,
						animalType: 1,
						sex: 1,
						thumbnailURL: 1,
						animalSize: 1,
						animalFur: 1,
						animalNumberColors: 1,
						color1: 1,
						color2: 1,
						color3: 1,
						aditionalDescription: 1,
						reported: 1
						})
				.sort({creationDate: 'desc'})
				.skip((Number(postsToSkip)))
				.limit(12)
				.populate('user', 'username name pictureURL -_id')
				.exec(function(err, results){
					if (err){						
						res.status(500).json({status: 500, data: null});
						return;
					}
					else{
						res.status(200).json({status: 200, data: results});
						return;
					}
				});			
		},
		
		unreportPost: function(req, res){
			//console.log("REPORT POST");
			var postID = req.body.postID;
			
			if (req.user == null){
				console.log("unreportPost", "Intenta operar con usuario null", "IP: " + LogQueries.getIP(req));
				res.status(400).json({status: 400, data: null});
				return;
			}
			
			if (req.user.access_level < 2){
				console.log("unreportPost", "Intenta operar con pocos privilegios", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username, "Post._id:" + postID);
				res.status(400).json({status: 400, data: null});
				return;
			}
			
			//LOG THE USER THAT REPORTS
			
			Post.update({ _id: postID },
						{ $set: { reported : false } },
						function (err){
							if (err) {
								res.status(500).json({status: 500, data: null});								
								return;
							}
							
							console.log("unreportPost", "Desreporta post exitosamente", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username, "Post._id:" + postID);
							res.status(200).json({status: 200, data: null});							
							return;
						}
						)
		}
		
	}
})();