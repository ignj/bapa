var mongoose = require('mongoose');

var User = mongoose.model('User');
var bcrypt = require('bcrypt-nodejs');
var salt = bcrypt.genSaltSync(10);
var LogQueries = require('../utils/logQueries');

//DEFINE ACCESS LEVELES
var BANNED_ACCESS = 0;		//CANT DO ANYTHING IN THE APP
var NORMAL_ACCESS = 1;		//NORMAL POSTING; DELETING; EDITING; REPORTING
var MOD_ACCESS = 2;			//ONLY DELETING OR APROBE POSTS
var SUPERMOD_ACCESS = 3;	//THE SAME OF MOD_ACCESS AND BAN / UNBAN USERS
var SEMIADMIN_ACCESS = 4;	//THE SAME OF SUPERMOD_ACCESS AND ADD NEW MOD_ACCESS AND SUPERMOD_ACCESS
var ADMIN_ACCESS = 5;		//ANYTHING

module.exports = (function(){
	return{		
		getUserData: function(req, res){
			//console.log("GETUSERDATA");
			
			var user = req.user;
			var attrUsername = req.query.username;
			
			if (user == null){
				console.log("getUserData", "Intenta operar user null", "IP: " + LogQueries.getIP(req));
				res.status(400).json({status: 400, data: null});				
				return;
			}

			if (user.access_level < SUPERMOD_ACCESS){
				console.log("getUserData", "Intenta operar sin privilegios", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username);
				res.status(400).json({status: 400, data: null});				
				return;
			}
			
			User.find({ username : attrUsername })
				.select({					
					username: 1,
					name: 1,
					pictureURL: 1,
					access_level: 1
				})
				.exec(function(err, results){
					if (err){
						res.status(500).json({status: 500, data: null});						
						return;
					}
					
					console.log("getUserData", "Obtiene informacion", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username, "User buscado:" + attrUsername);
					res.status(200).json({status: 200, data: results});
					return;
					
				});
			
		},
		
		setAccessLevel: function(req, res){
			console.log("SET ACCESS LEVEL");
			
			var user = req.user;
			var attrTargetID = req.body.userID;
			var attrTargetAccess = req.body.targetAccess;
			
			if (user == null){
				console.log("setAccessLevel", "Opera con user null", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username);
				res.status(400).json({status: 400, data: null});
				return;
			}
			
			if (user.access_level < SUPERMOD_ACCESS){
				console.log("setAccessLevel", "Opera sin privilegios", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username);
				res.status(400).json({status: 400, data: null});
				return;
			}
			
			if (user.access_level == SUPERMOD_ACCESS && attrTargetAccess > NORMAL_ACCESS ){
				console.log("setAccessLevel", "Opera sin privilegios", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username);
				res.status(400).json({status: 400, data: null});
				return;
			}
			
			if (user.access_level == SEMIADMIN_ACCESS && attrTargetAccess > SUPERMOD_ACCESS){
				console.log("setAccessLevel", "Opera sin privilegios", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username);
				res.status(400).json({status: 400, data: null});
				return;
			}
			
			/*
			if (user.access_level == ADMIN_ACCESS && validID(user._id)){
				console.log("setAccessLevel", "Opera sin privilegios", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username);
				res.status(400).json({status: 400, data: null});
				return;
			}*/
				
			
			//LOG USER THAT PERFORMS ACTION
			
			User.update({ _id: attrTargetID },
						{ $set: { access_level : attrTargetAccess } },
						function (err){
							if (err){
								res.status(500).json({status: 500, data: null});		
								return;
							}
							
							console.log("setAccessLevel", "Cambia exitosamente el nivel de acceso", "IP: " + LogQueries.getIP(req), "User._id: " + req.user._id, "User.username: " + req.user.username, " Usuario afectado: " + attrTargetID, " Nuevo acceso: " + attrTargetAccess);
							res.status(200).json({status: 200, data: null});							
							return;					
						}
						);
			
		}
	}
})();
