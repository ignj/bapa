var self = module.exports = (function(){
	
	return{
		
		getIP: function(request){
			return request.connection.remoteAddress;
		}
		
	}
	
})();