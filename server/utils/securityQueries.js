var self = module.exports = (function(){
	
	return{
	
		getValidPublicationTypes : function(){
			return ["Perdido", "Encontrado", "En adopción"];
		},
		
		getValidAnimalTypes : function(){
			return ["Perro", "Gato"];
		},
		
		getValidAnimalSexs : function(){
			return ["male", "female"];
		},
		
		getValidAnimalSizes : function(){
			return ["Pequeño", "Mediano", "Grande"];
		},
		
		getValidAnimalFur : function(){
			return ["Pelo corto", "Pelo largo"];
		},
		
		getValidAnimalNumberColors : function(){
			return [1, 2, 3];
		},
		
		getValidAnimalColors : function(){
			return ["Blanco", "Negro", "Gris", "Marron oscuro", "Marron claro / Naranja", "Otro"];
		},
		
		/** 
			Compares 2 mongoDB ObjectId
		**/
		isOwner: function(id1, id2){
			return id1.equals(id2);
		},
		
		isValidPublicationType: function(value){
			return self.getValidPublicationTypes().includes(value);
		},
		
		isValidAnimalType: function(value){
			return self.getValidAnimalTypes().includes(value);
		},
		
		isValidAnimalSex: function(value){
			return self.getValidAnimalSexs().includes(value);
		},
		
		isValidAnimalSize(value){
			return self.getValidAnimalSizes().includes(value);
		},
		
		isValidAnimalFur(value){
			return self.getValidAnimalFur().includes(value);
		},
		
		isValidAnimalNumberColors(value){			
			return self.getValidAnimalNumberColors().includes(Number(value));
		},
		
		isValidAnimalColor(value){			
			return self.getValidAnimalColors().includes(value) || (value == null);
		},
		
		isValidAditionalDescription(value){
			var numberOfCharacters = 100;
			if (value == null)
				return true;
			else
				return value.length <= numberOfCharacters;
		}
	}
	
})();