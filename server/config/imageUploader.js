var multer = require('multer');
var fs = require('fs');
var mkdirp = require('mkdirp');
var baseFolder = "./public";
var subFolder = "/uploads/";

function getDate(){
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!

	var yyyy = today.getFullYear();
	if(dd<10){
		dd='0'+dd;
	} 
	if(mm<10){
		mm='0'+mm;
	} 
	return today = dd+'-'+mm+'-'+yyyy;
}

var storage = multer.diskStorage({
		destination: function (req, file, cb) {
			//console.log(req.body);
			var absoluteDir = baseFolder + subFolder + req.body.locationFolder;			
			mkdirp.sync(absoluteDir);
			cb(null, absoluteDir);
		},
		filename: function (req, file, cb) {			
			var extArray = file.mimetype.split("/");
			var extension = extArray[extArray.length - 1];
			cb(null, req.body.id + '_' + getDate() + '.' +extension);
		}
	});
var upload = multer({ storage: storage })

module.exports = function(app) {
	
	app.post('/upload/', upload.single('file'), function (req, res, next) {
		// req.file is the `avatar` file
		// req.body will hold the text fields, if there were any
		if (req.file){
			//remove public path
			var originalPath = req.file.path;
			var relativePath = req.file.path.substring(6);
			//console.log(originalPath);
			//console.log(relativePath);
			res.send(relativePath);			
		}
	});
}