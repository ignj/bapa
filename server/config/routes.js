var users = require('../controllers/users');
var posts = require('../controllers/posts');
var geolocations = require('../controllers/geolocations');
var passport = require('passport');

module.exports = function(app) {
  
	//OAUTH routes
	app.get("/auth/facebook", passport.authenticate("facebook"));
	app.get("/auth/facebook/callback",
			passport.authenticate("facebook",
			{ failureRedirect: "/", successRedirect:"/" })
		);

	app.post('/logout', function(req,res){
		//console.log('logging out ', req.session.passport.user);
		req.logOut();
		//console.log('are they still logged in?', req.isAuthenticated());
		res.send(200);
	})
  
	//parameters
	app.param('post', posts.getPostById);

	//route to test if the user is logged in or not
	app.get('/loggedin', function(req, res){
		//console.log('loggedin?', req.isAuthenticated());
		return res.json(req.isAuthenticated() ? req.user : null);
	});  
  
	app.post('/posts', posts.createPost );
	app.get('/posts', posts.getPosts );
	app.get('/posts/:post', posts.removeUserInformation );  
	app.patch('/posts/:post', posts.patchPost );
	app.delete('/posts/:post', posts.deletePost );
  
	app.get('/search', posts.searchPosts );
	app.get('/geolocation', geolocations.getGeolocation );
  
	app.post('/reported', posts.reportPost );
	app.get('/reported', posts.getReportedPosts );
	app.patch('/reported', posts.unreportPost );
  
	app.get('/users', users.getUserData );
	app.patch('/users', users.setAccessLevel );

};
