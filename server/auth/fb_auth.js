var mongoose = require("mongoose");
var User = mongoose.model("User");
var passport = require("passport");
var FacebookStrategy = require("passport-facebook").Strategy;

var FACEBOOK_APP_ID = "1328756237167142";
var FACEBOOK_APP_SECRET = "b0e7afd0a0e267142f2c4c393a19554b";
passport.use(new FacebookStrategy({
				clientID: FACEBOOK_APP_ID,
				clientSecret: FACEBOOK_APP_SECRET,
				callbackURL: "/auth/facebook/callback"
			},
			
			function(accessToken, refreshToken, profile, done) {
					//console.log('fbauth');					
					User.findOne({ authId: profile.id }, function(err, user) {
						if(err) {
							return done(err);
						}

						if(!user) {
							user = new User({
								authId: profile.id,
								username: profile._json.id, //should change this later
								name: profile.displayName,
								provider: profile.provider,
								pictureURL: 'https://graph.facebook.com/'+profile._json.id+'/picture?type=large',
								json_info: profile._json,
								access_level: Number(1)
							});
							user.save(function(err) {
								if(err) {
									console.log(err)
								}
								return done(err, user);
							});
						} 
						else {							
							return done(err, user);
						}
					})
			}
));
