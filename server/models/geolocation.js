var mongoose = require('mongoose');

var geolocationSchema = new mongoose.Schema({
	city: String,
	countie: String,
	state: String,
	country: String
});

mongoose.model('Geolocation', geolocationSchema);

/* CREATED IN CLIENT/JS/FACTORY/GEOLOCATIONSVC
'{ "city":\"'+results[0].address_components[2].long_name+'\" , '
+' "countie":\"'+results[0].address_components[3].long_name+'\" , '
+' "state":\"'+results[0].address_components[4].long_name+'\" , '
+' "country":\"'+results[0].address_components[5].long_name+'\" }
*/