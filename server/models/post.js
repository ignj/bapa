var mongoose = require('mongoose');
var geolocation = mongoose.model('Geolocation');

var postSchema = new mongoose.Schema({
	creationDate: { type: Date, expires: '7d', default: Date.now },
	geolocation: { type: mongoose.Schema.Types.ObjectId, ref: 'Geolocation' },
	user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
	publicationType: String,
	animalType: String,
	sex: String,
	highResURL: String,
	thumbnailURL: String,
	animalSize: String,
	animalFur: String,
	animalNumberColors: { type: Number, min: 1, max: 3 },
	color1: String,
	color2: String,
	color3: String,
	aditionalDescription: String,
	reported: Boolean
});

mongoose.model('Post', postSchema);